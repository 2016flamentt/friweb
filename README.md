# Projet Recherche Info-WEB

Par Thomas FLAMENT et Valentin LEFRANC

## Instructions de compilation

- Télécharger la [collection CS276](http://web.stanford.edu/class/cs276/pa/pa1-data.zip) dans le dossier `data/CS276` de sorte que `CS276` contienne `pa1-data`.

## Questions

### Partie 1 : Traitements linguistiques

|  | CACM | CS 276 |
| ---- | ---- | ---- |
| Nombre de Tokens | 118 203 | 17 759 906 |
| Taille du Vocabulaire | 9 497 | 309 742 |
| Nombre de Tokens (moitié de la collection) | 30 612 | 9 875 014 |
| Taille du Vocabulaire (moitié de la collection) | 5 294 | 179 546 |
| Loi de Heap : *k* | 60.72 | 0.057 |
| Loi de Heap : *b* | 0.433 | 0.929 |
| Taille du Vocabulaire estimée pour 1 million de tokens | 23 918 | 21 388 |

![](cacm_rank.png)
![](cacm_rank_log.png)

![](cs276_rank.png)
![](cs276_rank_log.png)

### Partie 2 : Modèles de recherches booléens et vectoriels

Trois variables sont stockées pour permettre la recherche :
- `terms`, un dictionnaire indexé par nom de terme, ayant pour valeur l'id de chaque terme ;
- `documents`, un tableau indexé par l'identifiant de chaque document, ayant pour valeur un tuple (id du document, norme du document) ;
- `index`, un tableau indexé par id de terme, ayant pour valeur un tableau (de postings) sous forme de tuples (id du document, fréquence du terme dans le document).

#### Modèle de recherche booléen

On utilise ici une bibliothèque permettant le parsage d'une chaine de caractères représentant une formule booléenne, par exemple "mean AND efficient".
La requête est donc dans ce format : par exemple, "mean AND efficient".

#### Modèle de recherche vectoriel

Puisque la recherche s'effectue par cosinus-similarité, à un moment donné il faut que la requête puisse être exprimable comme un document.
Pour cela, la requête est initialement sous la forme de texte libre (phrases), puis est converti en document en établissant un dictionnaire associant à chaque terme sa fréquence (et ignorant les termes n'apparaissant pas dans le vocabulaire du corpus).
Le moteur renvoie tous les résultats de score non nul, sous forme de liste ordonnée décroissante de tuples (id du document, score du document).

### Partie 3 : Evaluation

#### Performance :
L'index est créé en 1.22 secondes.

L'index CACM a une taille de 2M.

Réponse en 745 microsecondes pour une requête booléenne.

Réponse en 59.6 microsecondes pour une requête vectorielle.

#### Précision :

Courbes Rappel-Précision, E-Measure et F-Measure
![](precision_curve.png)

On obtient un Mean Average Precision de 0.2656