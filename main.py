#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import math
import numpy as np
import boolean
import time
import matplotlib.pyplot as plt
import itertools
import re
import os
import tempfile
import json
import pprint
from hurry.filesize import size


"""
2.1 Traitements linguistiques
"""
tokenize_regex = re.compile("[^A-Za-z0-9]+")
def tokenize(line):
    return tokenize_regex.split(line)

with open('data/CACM/common_words') as file:
    stop_words = [line.rstrip('\n') for line in file]
def stop(tokens):
    r = []
    for token in tokens:
        if not token:
            continue
        t = token.lower()
        if t in stop_words:
            continue
        r.append(t)
    return r

def blocks_cacm():
    def block():
        with open("data/CACM/cacm.all") as cacm:
            document_id = -1
            document_words = []
            skip = True
            for line in cacm:
                line = line.rstrip('\n')
                if line.startswith(".I"):
                    skip = False
                    if document_id != -1:
                        yield (document_id, document_words)
                    document_id = line[len(".I "):]
                    document_words = []
                elif line.startswith(".T") or line.startswith(".W") or line.startswith(".K"):
                    skip = False
                elif line.startswith(".B") or line.startswith(".A") or line.startswith(".N") or line.startswith(".X"):
                    skip = True
                else:
                    if not skip:
                        document_words.extend(stop(tokenize(line)))
            if document_id != -1:
                yield (document_id, document_words)
    return [block()]

def blocks_cs276():
    def block(i):
        for e in os.listdir(f"data/CS276/pa1-data/{i}/"):
            with open(f"data/CS276/pa1-data/{i}/{e}") as f:
                document_words = stop(f.readline().rstrip('\n').split())
            yield (f"{i}/{e}", document_words)
    return [block(i) for i in range(10)]

"""
2.1.3 & 4 Calculs paramètres Heap Law et taille du vocabulaire
"""
def HeapLaw(nb_tokens, nb_voc):
    #    Calculate (b,k) from Heap law: voc = k * tokens ^ b
    regression = np.polyfit(np.log10(nb_tokens), np.log10(nb_voc), deg=1)
    b = regression[0]
    k = 10 ** regression[1]
    return [b, k]


def calcul_voc(b,k, size):
    return k * (size ** b)

"""
2.2 Création Index
"""

def make_index(blocks, weight='tf-idf'):
    terms = {}
    documents = []
    file_names = []
    documents_frequencies = []
    max_term_frequencies = []
    for block in blocks:
        pairs = []
        for document_name, document_terms in block:
            document_id = len(documents)
            documents.append((document_name, 0))
            max_term_frequencies.append(0)
            for term in document_terms:
                if term not in terms:
                    term_id = len(terms)
                    documents_frequencies.append(0)
                    terms[term] = term_id
                else:
                    term_id = terms[term]
                pairs.append((term_id, document_id))
        pairs.sort(key=lambda x:x[0])
        temp_file = tempfile.NamedTemporaryFile(mode="w+", delete=False)
        for term_id, document_ids in itertools.groupby(pairs, key=lambda x: x[0]):
            temp_file.write(f"{term_id}")
            for document_id, ids in itertools.groupby(document_ids, key=lambda x: x[1]):
                term_frequency = sum(1 for _ in ids)
                temp_file.write(f"|{document_id},{term_frequency}")
                documents_frequencies[term_id] += 1
            temp_file.write("\n")
        temp_file.close()
        file_names.append(temp_file.name)
    index = [[] for _ in range(len(terms))]
    for file_name in file_names:
        with open(file_name) as file:
            for line in file:
                parts = line.split(sep="|")
                if not parts:
                    continue
                term_id = int(parts[0])
                for document in parts[1:]:
                    document_parts = document.split(sep=",")
                    document_id = int(document_parts[0])
                    term_frequency = int(document_parts[1])
                    if term_frequency > max_term_frequencies[document_id]:
                        max_term_frequencies[document_id] = term_frequency
                    index[term_id].append((document_id, term_frequency))
        os.remove(file_name)
    if weight == 'tf-idf' or weight == 'tf-idf-norm':
        for term_id in range(len(index)):
            for i in range(len(index[term_id])):
                document_id, term_frequency = index[term_id][i]
                term_weight = (1 + math.log10(term_frequency)) * math.log10(len(documents) / documents_frequencies[term_id])
                index[term_id][i] = (document_id, term_weight)
                document_name, document_norm = documents[document_id]
                documents[document_id] = (document_name, document_norm + term_weight ** 2)
        for i in range(len(documents)):
            document_name, document_norm = documents[i]
            documents[i] = (document_name, math.sqrt(document_norm))
        if weight == 'tf-idf-norm':
            for term_id in range(len(index)):
                for i in range(len(index[term_id])):
                    document_id, term_weight = index[term_id][i]
                    index[term_id][i] = (document_id, term_weight / documents[i][1])
    elif weight == 'freq':
        for term_id in range(len(index)):
            for i in range(len(index[term_id])):
                document_id, term_frequency = index[term_id][i]
                term_weight = term_frequency / max_term_frequencies[document_id]
                index[term_id][i] = (document_id, term_weight)
                document_name, document_norm = documents[document_id]
                documents[document_id] = (document_name, document_norm + term_weight ** 2)
        for i in range(len(documents)):
            document_name, document_norm = documents[i]
            documents[i] = (document_name, math.sqrt(document_norm))
    else:
        raise ValueError(f"unexpected weight method {weight}, allowed values are: tf-idf, tf-idf-norm, freq")
    return terms, documents, index


"""
2.2.1 Modèle de recherche booléen
"""
request_regex = re.compile(" |([()&|~])")
algebra = boolean.BooleanAlgebra()


def boolean_request(terms, documents, index, request):
    def inter(l1, l2):
        l = []
        i = 0
        j = 0
        while i < len(l1) and j < len(l2):
            if l1[i] < l2[j]:
                i += 1
            elif l1[i] > l2[j]:
                j += 1
            else:
                l.append(l1[i])
                i += 1
                j += 1
        return l

    def union(l1, l2):
        l = []
        i = 0
        j = 0
        while i < len(l1) and j < len(l2):
            if l1[i] < l2[j]:
                l.append(l1[i])
                i += 1
            else:
                l.append(l2[j])
                j += 1
        while i < len(l1):
            l.append(l1[i])
            i += 1
        while j < len(l2):
            l.append(l2[j])
            j += 1
        return l

    def exclude(l1, l2):
        l = []
        i = 0
        j = 0
        while i < len(l1) and j < len(l2):
            if l1[i] < l2[j]:
                l.append(l1[i])
                i += 1
            elif l1[i] > l2[j]:
                j += 1
            else:
                i += 1
                j += 1
        while i < len(l1):
            l.append(l1[i])
            i += 1
        return l

    def run_and(node1_documents, node1_include, node2_documents, node2_include):
        if node1_include and node2_include:
            return inter(node1_documents, node2_documents), True
        if not node1_include and not node2_include:
            return union(node1_documents, node2_documents), False
        if not node1_include:
            node1_documents, node2_documents = node2_documents, node1_documents
        return exclude(node1_documents, node2_documents), True

    def run_or(node1_documents, node1_include, node2_documents, node2_include):
        if node1_include and node2_include:
            return union(node1_documents, node2_documents), True
        if not node1_include and not node2_include:
            return inter(node1_documents, node2_documents), False
        if not node1_include:
            node1_documents, node2_documents = node2_documents, node1_documents
        return exclude(node2_documents, node1_documents), False

    def run(node):
        if isinstance(node, boolean.Symbol):
            term_id = terms[node.obj.lower()]
            return [posting[0] for posting in index[term_id]], True
        elif isinstance(node, boolean.NOT):
            node_documents, include = run(node.args[0])
            return node_documents, not include
        elif isinstance(node, boolean.AND):
            node_documents, include = run_and(*run(node.args[0]), *run(node.args[1]))
            for i in range(2, len(node.args)):
                node_documents, include = run_and(node_documents, include, *run(node.args[i]))
            return node_documents, include
        elif isinstance(node, boolean.OR):
            node_documents, include = run_or(*run(node.args[0]), *run(node.args[1]))
            for i in range(2, len(node.args)):
                node_documents, include = run_or(node_documents, include, *run(node.args[i]))
            return node_documents, include
        else:
            raise ValueError(f'unexpected node {node} of type {type(node)}')

    document_ids, include = run(algebra.parse(request))
    if not include:
        document_ids_include = []
        k = -1
        for document_id in document_ids:
            for i in range(k+1, document_id):
                document_ids_include.append(i)
            k = document_id
        for i in range(k + 1, len(documents)):
            document_ids_include.append(i)
        document_ids = document_ids_include

    return [documents[document_id][0] for document_id in document_ids]

"""
2.2.1 Modèle de recherche vectoriel
"""
def vector_request(terms, documents, index, request, weight='tf-idf'): # request is a list of words
    scores = {}
    request_terms = {}
    for word in request:
        if word not in terms:
            continue
        request_terms[word] = request_terms.get(word, 0) + 1

    request_weights = {}
    request_norm = 0
    if weight == 'tf-idf' or weight == 'tf-idf-norm':
        for term_name, request_frequency in request_terms.items():
            term_id = terms[term_name]
            df_weight = math.log10(len(documents) / len(index[term_id]))
            tf_weight = 1 + math.log10(request_frequency)
            request_weight = df_weight * tf_weight
            request_weights[term_id] = request_weight
            request_norm += request_weight ** 2
        request_norm = math.sqrt(request_norm)
        if weight == 'tf-idf-norm':
            for term_id in request_weights.keys():
                request_weights[term_id] /= request_norm
    elif weight == 'freq':
        request_max_frequency = 0
        for request_frequency in request_terms.values():
            if request_frequency > request_max_frequency:
                request_max_frequency = request_frequency
        for term_name, request_frequency in request_terms.items():
            term_id = terms[term_name]
            request_weight = request_frequency / request_max_frequency
            request_weights[term_id] = request_weight
            request_norm += request_weight ** 2
        request_norm = math.sqrt(request_norm)

    for term_name, request_frequency in request_terms.items():
        term_id = terms[term_name]
        request_weight = request_weights[term_id]
        for document_id, term_weight in index[term_id]:
            scores[document_id] = scores.get(document_id, 0) + term_weight * request_weight
    for document_id in scores.keys():
        scores[document_id] /= documents[document_id][1] * request_norm

    scores_sorted = sorted(scores.items(), key=lambda x: -x[1])
    return [(documents[document_id][0], score) for document_id, score in scores_sorted]

"""
2.3 Evaluation pour la collection CACM
"""

""" Evaluation de la performance : temps de création d'un index, de temps de réponse à une requête booléenne / vectorielle, 
taille de l'index"""

def index_creation_time():
    start = time.time()
    make_index(blocks_cacm(), weight='tf-idf')
    end = time.time()
    return end-start

def boolean_response_time():
    start = time.time()
    boolean_request(terms, documents, index, "computer AND NOT algorithm")
    end = time.time()
    return end - start

def vectorial_response_time():
    start = time.time()
    vector_request(terms, documents, index, ["interleaved", "mean"], weight='tf-idf')
    end = time.time()
    return end - start

def file_size_index():
    terms, documents, index = make_index(blocks_cacm(), weight='tf-idf')
    with open('index.json', 'w') as outfile:
        json.dump(index, outfile)
    return size(os.path.getsize("index.json"))

""" Evaluation de la pertinence : precision, rappel, F mesure, E mesure, R mesure, Mean average precision"""

def parse_queries():
    queries = {}
    with open("data/CACM/query.text") as cacm:
        query_id = -1
        query_words = []
        skip = True
        for line in cacm:
            line = line.rstrip('\n')
            if line.startswith(".I"):
                skip = False
                if query_id != -1:
                    queries[query_id] = (query_words, [])
                query_id = line[len(".I "):]
                query_words = []
            elif line.startswith(".W"):
                skip = False
            elif line.startswith(".A") or line.startswith(".N"):
                skip = True
            else:
                if not skip:
                    query_words.extend(stop(tokenize(line)))
        if query_id != -1:
            queries[query_id] = (query_words, [])
    with open("data/CACM/qrels.text") as cacm:
        for line in cacm:
            line = line.lstrip('0')
            query, document, _, _ = line.split()
            queries[query][1].append(document)
    return queries


def test_CACM_qrels():
    queries = parse_queries()

    expected = {key : [] for key in range(1, 65)}
    actual = {key : [] for key in range(1, 65)}
    i = 0
    for words, documents_related in queries.values():
        i += 1
        if not documents_related:
            continue
        documents_returned = [x[0] for x in vector_request(terms, documents, index, words, weight='tf-idf')]
        actual[i] = documents_returned
        expected[i] = documents_related
    return expected, actual


def plot_recall_precision(expected, actual):
    nb_points = 11  # 11 points pour l'espace [0,1]
    x = [0.1*n for n in range(nb_points)]  # recall
    y = [0.0 for _ in range(nb_points)]  # precision
    e = [0.0 for _ in range(nb_points)]  # e-measure
    f = [0.0 for _ in range(nb_points)]  # f-measure
    avg_precision = []

    for request_id in expected.keys():
        rappel_list, precision_list = calculate_recall_precision(expected[request_id], actual[request_id])
        avg_precision.append(np.average(precision_list))
        for j in range(nb_points):
            rj = x[j]
            for i, r in enumerate(rappel_list):
                if r > rj:
                    y[j] += max(precision_list[i:])
                    break
    for j in range(nb_points):
        y[j] = y[j]/len(actual.keys())

    # calculate e and f measure
    for j in range(nb_points):
        e[j] += e_measure(x[j], y[j])
        f[j] += 1 - e[j]

    plt.plot(x, y, marker="x", label='Precision')
    plt.plot(x, e, marker='x', label="e-measure")
    plt.plot(x, f, marker='x', label="f-measure")
    plt.axis('equal')
    plt.xlabel('Recall')
    plt.legend()
    plt.title("Recall-precision curve")
    plt.show()
    print("The Mean Average Precision is {}".format(np.nanmean(avg_precision)))

def calculate_recall_precision(expected, actual):
    recall = []
    precision = []
    relevant_results = []
    for rank in range(len(actual)):
        if actual[rank] in expected:
            relevant_results.append(actual[rank])
            recall.append(len(relevant_results)/len(expected))
            precision.append(len(relevant_results)/(rank+1))
    return recall, precision

def calculate_r_measure(expected, actual):
    for request_id in expected.keys():
        r_prec = r_measure(expected[request_id], actual[request_id])
        print("{0} : the r-precision is {1}.". format(request_id, r_prec))

def r_measure(expected, actual):
    r_prec = 0
    for rank in range(len(expected)):
        if rank < len(actual) and actual[rank] in expected:
            r_prec += 1
    if len(expected) == 0 :
        return 0
    else :
        return r_prec/len(expected)

def e_measure(rappel, precision):
    if rappel > 0:
        beta = float(precision) / float(rappel)
        e = 1 - (((beta * beta + 1) * rappel * precision) / (beta * beta * precision + rappel))
        return e
    else:
        return 1

def calculate_precision():
    expected, actual = test_CACM_qrels()
    plot_recall_precision(expected, actual, )
    calculate_r_measure(expected, actual)


terms, documents, index = make_index(blocks_cacm(), weight='tf-idf')

"""
print("List of tuples (document_name, document_norm) indexed by document_id :")
pprint.pprint(index)

print("List of tuples (document_name, document_norm) indexed by document_id :")
pprint.pprint(documents) 

print("Map of tuples (term_name, term_id), indexed by term_name :")
pprint.pprint(terms)

#Calcul of Heap Law Parameters and vocabulary
nb_tokens_CACM = 118203
nb_tokens_CACM_half = 30612

nb_tokens_CS276 = 17759906
nb_tokens_CS276_half = 9875014

nb_voc_CACM = 9497
nb_voc_CACM_half = 5294

nb_voc_CS276 = 309742
nb_voc_CS276_half = 179546

# CACM parameters
tokensCACM = np.array([nb_tokens_CACM, nb_tokens_CACM_half])
vocCACM = np.array([nb_voc_CACM, nb_voc_CACM_half])

# CS276 parameters
tokensCS276 = np.array([nb_tokens_CS276,  nb_tokens_CS276_half])
vocCS276 = np.array([nb_voc_CS276, nb_voc_CS276_half])

HeapCACM = HeapLaw(tokensCACM, vocCACM)
HeapCS276 = HeapLaw(tokensCS276, vocCS276)

print("Heap Law parameters :")
print("For CACM : (b, k) = {}, and for 1 millions tokens, there would be a vocabulary of {}".
      format(HeapCACM,calcul_voc(HeapCACM[0], HeapCACM[1], 1000000)))
print("For CS276 : (b, k) = {}, and for 1 millions tokens, there would be a vocabulary of {}".
      format(HeapCS276, calcul_voc(HeapCS276[0], HeapCS276[1], 1000000)))
    
#Boolean
print("List of document names for the request 'computer AND NOT algorithm' :")
pprint.pprint(boolean_request(terms, documents, index, "computer AND NOT algorithm"))

#Vectorial
print("List of tuples (document names, relative score) for the request '['interleaved', 'mean']' :")
pprint.pprint(vector_request(terms, documents, index, ["interleaved", "mean"], weight='tf-idf'))

# Performance
print("The index is created in {} seconds.".format(index_creation_time()))
print("Size of the CACM index : {}.".format(file_size_index()))
print("Response in {} seconds for a boolean request.".format(boolean_response_time()))
print("Response in {} seconds for a vectorial request.".format(vectorial_response_time()))

# Precision
calculate_precision()

"""
